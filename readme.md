# API Info for Service Provider Pro (SPP.co)

[SPP.co](https://spp.co) is the all-in-one client management & billing software for agencies selling productized services. Use these API functions to manage invoices, orders, and clients in your SPP worskpace.

Please note that the API is available to users on the [Enterprise plan](https://spp.co/pricing). For other use cases you can also check out our [Zapier integration](https://spp.co/help/knowledgebase/zapier/).

## Authentication

SPP uses HTTP Basic Auth for authentication. Your basic auth username is your secret API key. You can view and manage your API key in the API module settings.

## Errors

Our API uses conventional HTTP response codes to indicate the success or failure of an API request.

-   `200` code indicates that request was successful.
-   `400` code indicates an error with the request, such as missing required fields or invalid data.
-   `403` code indicates an authentication error, for example, wrong API key.
-   `500` code indicates an issue on our end.

## Curently Available API Actions

We can add endpoints and actions on an as-needed basis. Please [contact support](https://spp.co/help/contact/) if your use case isn't supported yet.

-   [Clients](#markdown-header-clients)
    -   [Create a client](#markdown-header-create-a-client)
    -   [Retrieve a client](#markdown-header-retrieve-a-client)
    -   [Update a client](#markdown-header-update-a-client)
    -   [Delete a client](#markdown-header-delete-a-client)
    -   [List all clients](#markdown-header-list-all-clients)
-   [Invoices](#markdown-header-invoices)
    -   [Create an invoice](#markdown-header-create-an-invoice)
    -   [Retrieve an invoice](#markdown-header-retrieve-an-invoice)
    -   [Update an invoice](#markdown-header-update-an-invoice)
    -   [Charge an invoice](#markdown-header-charge-an-invoice)
    -   [Mark an invoice as paid](#markdown-header-mark-an-invoice-as-paid)
    -   [Delete an invoice](#markdown-header-delete-an-invoice)
    -   [List all invoices](#markdown-header-list-all-invoices)
-   [Orders](#markdown-header-orders)
    -   [Create an order](#markdown-header-create-an-order)
    -   [Retrieve an order](#markdown-header-retrieve-an-order)
    -   [Update an order](#markdown-header-update-an-order)
    -   [Delete an order](#markdown-header-delete-an-order)
    -   [List all orders](#markdown-header-list-all-orders)
-   [Order Messages](#markdown-header-order-messages)
    -   [Create an order message](#markdown-header-create-an-order-message)
    -   [Delete an order message](#markdown-header-delete-an-order-message)
    -   [List all order messages](#markdown-header-list-all-order-messages)
-   [Tickets](#markdown-header-tickets)
    -   [Create a ticket](#markdown-header-create-a-ticket)
    -   [Retrieve a ticket](#markdown-header-retrieve-a-ticket)
    -   [Update a ticket](#markdown-header-update-a-ticket)
    -   [Delete a ticket](#markdown-header-delete-a-ticket)
    -   [List all tickets](#markdown-header-list-all-tickets)
-   [Ticket Messages](#markdown-header-ticket-messages)
    -   [Create a ticket message](#markdown-header-create-a-ticket-message)
    -   [Delete a ticket message](#markdown-header-delete-a-ticket-message)
    -   [List all ticket messages](#markdown-header-list-all-ticket-messages)
-   [Login Links](#markdown-header-login-links)
    -   [Retrieve a login link](#markdown-header-retrieve-a-login-link)

## Clients

Client accounts are created automatically during the checkout process in SPP. Users can also sign up by visiting your signup page at `/signup`. The API allows you to create and update client accounts. Successful client creation or update will return the client object.

### Create a client

As with manual signup, creating a client account via the API triggers a welcome email. It contains a randomly generated password if no password was specified during account creation.

```
curl https://username.spp.io/api/v1/clients \
    -u YOUR_API_KEY: \
    -d 'email=client@example.com' \
    -d 'password=password123' \
    -d 'name_f=John' \
    -d 'name_l=Smith' \
    -d 'company=Acme Inc' \
    -d 'tax_id=EE123456789' \
    -d 'phone=123 456 7890' \
    -d 'address[line_1]=123 N Street' \
    -d 'address[city]=Sacramento' \
    -d 'address[state]=CA' \
    -d 'address[postcode]=95502' \
    -d 'address[country]=US' \
    -d 'note=This is a test client' \
    -d 'optin=1' \
    -d 'stripe_customer_id=cus_abCdEf' \
    -d 'date_added=2020-12-31' \
    -d 'custom_fields[123]=Value of custom field #123'
```

#### Arguments

**`email`** (required)
User's email address. If an account with this email exists a validation error will be returned.

**`password`**
Include this field to set a user's password, or leave it out to generate a random one.

**`name_f`**
Client's first name.

**`name_l`**
Client's last name.

**`company`**
Client's company name. Will be shown in orders list, and autofilled during checkout.

**`tax_id`**
Client's tax ID. Will be autofilled during checkout if you're charging tax.

**`name_l`**
Client's phone.

**`address[]`**
Client's billing address. Will be autofilled in checkout if you have the address field enabled. Please use ISO 3166 two-letter codes for countries.

**`note`**
Account's note visible to your team.

**`optin`**
Whether the user has opted to receive marketing emails through your email marketing integration (MailChimp, ActiveCampaign, Aweber). Users are subscribed by default, only pass `1` or `0` if received explicit permission or denial.

**`stripe_customer_id`**
Import a client with an existing Stripe customer ID so they can use their saved payment methods when buying from you.

**`date_added`**
If you're importing an existing client you can set a different creation date for the account. Must be in `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` format, UTC timezone. For example, `2022-12-31 23:59:59`.

**`custom_fields[]`**
Array of custom field values. You can see your custom field IDs in Settings > CRM fields.

### Retrieve a client

You only need the client's ID.

```
curl https://username.spp.io/api/v1/clients/123 \
    -u YOUR_API_KEY:
```

#### Arguments

**`client`** (required)

### Update a client

Any parameters not provided will be left unchanged.

```
curl https://username.spp.io/api/v1/clients/123 \
    -u YOUR_API_KEY: \
    -d 'name_f=John' \
    -d 'name_l=Smith' \
    -d 'company=Acme Inc' \
    -d 'tax_id=EE123456789' \
    -d 'phone=123 456 7890' \
    -d 'address[line_1]=123 N Street' \
    -d 'address[city]=Sacramento' \
    -d 'address[state]=CA' \
    -d 'address[postcode]=95502' \
    -d 'address[country]=US' \
    -d 'note=This is a test client' \
    -d 'optin=1' \
    -d 'stripe_customer_id=cus_abCdEf' \
    -d 'date_added=2020-12-31' \
    -d 'custom_fields[123]=Value of custom field #123'
```

#### Arguments

**`client`** (required)

**`email`** (required)
User's email address. If an account with this email exists a validation error will be returned.

**`name_f`**
Client's first name.

**`name_l`**
Client's last name.

**`company`**
Client's company name. Will be shown in orders list, and autofilled during checkout.

**`tax_id`**
Client's tax ID. Will be autofilled during checkout if you're charging tax.

**`name_l`**
Client's phone.

**`address[]`**
Client's billing address. Will be autofilled in checkout if you have the address field enabled. Please use ISO 3166 two-letter codes for countries.

**`note`**
Account's note visible to your team.

**`optin`**
Whether the user has opted to receive marketing emails through your email marketing integration (MailChimp, ActiveCampaign, Aweber). Users are subscribed by default, only pass `1` or `0` if received explicit permission or denial.

**`stripe_customer_id`**
Import a client with an existing Stripe customer ID so they can use their saved payment methods when buying from you.

**`date_added`**
If you're importing an existing client you can set a different creation date for the account. Must be in `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` format, UTC timezone. For example, `2022-12-31 23:59:59`.

**`custom_fields[]`**
Array of custom field values. You can see your custom field IDs in Settings > CRM fields.

### Delete a client
Permanently deletes a client, their orders, tickets, and messages. Invoices are kept for accounting purposes. This action cannot be undone.

```
curl https://username.spp.io/api/v1/clients/2 \
    -u YOUR_API_KEY: \
    -X DELETE
```

### List all clients

```
curl https://username.spp.io/api/v1/clients?limit=3 \
    -u YOUR_API_KEY:
```

#### Arguments

**`email`**
Filter the list based on customer's email.

**`status`**
Filter the list based on customer's status field (`lead` or `client`).

**`limit`**
The maximum number of client objects returned.

**`offset`**
Used for pagination. For instance, if a maximum of 20 objects are returned (default), you can request the second page by setting `offset` to 20, third page by setting it to 40 and so on.


## Invoices

Payments and reporting in SPP are based around invoices. While we create invoices for payment automatically, you can use the API to add a manual invoice, and retrieve existing invoices. When retrieving an invoice, the invoice object is returned. When listing invoices, an array of invoice objects is returned.

### Create an invoice

Programmatically create an invoice for a customer. If no account exists a new one will be created with the provided email.

```
curl https://username.spp.io/api/v1/invoices \
    -u YOUR_API_KEY: \
    -d 'email=client@example.com' \
    -d 'date_due=2020-12-31' \
    -d 'note=This is a test invoice' \
    -d 'item_name[0]=Test item' \
    -d 'item_description[0]=Test description' \
    -d 'item_quantity[0]=2' \
    -d 'item_price[0]=1.99' \
    -d 'item_discount[0]=0.3' \
    -d 'item_service_id[0]=1' \
    -d 'item_name[1]=Test item 2' \
    -d 'item_price[1]=10.00' \
    -d 'item_quantity[1]=3'
```

#### Arguments

**`email`**
Client's email address. If an account with this email doesn't exist it will be created.

**`user_id`**
Client's ID, must exist in SPP. Without `email` or `user_id` we'll create a guest invoice which can be paid by any user.

**`date_due`**
Invoice's due date. Must be in `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` format, UTC timezone. For example, `2022-12-31 23:59:59`.

**`note`**
Invoice note, visible to client.

**`item_name[]`** (required)
Array of invoice item names.

**`item_description[]`**
Array of invoice item descriptions.

**`item_quantity[]`** (required)
Array of invoice item quantities.

**`item_price[]`** (required)
Array of invoice item prices.

**`item_discount[]`**
Array of invoice item discounts.

**`item_service_id[]`**
Array of invoice item service IDs. Items without a service ID are treated as custom items.

**`coupon_id`**
ID of discount code to apply. Not compatible with `item_discount` field.

**`status`**

* `0` – Pending (default). This is the initial status for new invoices.
* `1` – Paid. Adding a paid invoice does not trigger new orders in the system. Call `charge` or `mark_paid` endpoints for that.
* `2` – Overdue. Applied automatically to unpaid invoices.
* `3` – Unpaid. Initial status for manually issued invoices.
* `4` – Refunded. Applied on a full refund.


### Retrieve an invoice

You only need the invoice ID.

```
curl https://username.spp.io/api/v1/invoices/666AF315 \
    -u YOUR_API_KEY:
```

### Update an invoice

This endpoint uses the same arguments as invoice creation endpoint. Any parameters not provided will be left unchanged.

```
curl https://username.spp.io/api/v1/invoices/666AF315 \
    -u YOUR_API_KEY: \
    -d 'date_due=2020-12-31' \
```

Returns the invoice object.

### Charge an invoice

Charge an invoice using a saved payment method (works with [Stripe](https://spp.co/help/knowledgebase/stripe/)). Charging an invoice marks it as `Paid` and creates one or more orders in the system, depending on how many services were purchased.

```
curl https://username.spp.io/api/v1/invoices/666AF315/charge \
    -u YOUR_API_KEY: \
    -d 'payment_method_id=pm_xxx'
```

If the customer associated with an invoice doesn't have any payment methods in Stripe they can make a payment at `/pay/invoice/666AF315`. Their details will be automatically saved for future purchases.

#### Arguments

**`payment_method_id`**
Optional payment method ID from Stripe. If not provided we'll charge the default payment method.

### Mark an invoice as paid

If you've received a payment outside of SPP you can mark an invoice as paid. This will create new orders in your dashboard just like charging an invoice would.

```
curl https://username.spp.io/api/v1/invoices/666AF315/mark_paid \
    -u YOUR_API_KEY: \
    -X POST
```

### Delete an invoice
Permanently deletes an invoice. This action cannot be undone.

```
curl https://username.spp.io/api/v1/invoices/666AF315 \
    -u YOUR_API_KEY: \
    -X DELETE
```

### List all invoices

```
curl https://username.spp.io/api/v1/invoices?limit=3 \
    -u YOUR_API_KEY:
```

#### Arguments

**`limit`**
The maximum number of invoice objects returned.

**`offset`**
Used for pagination. For instance, if a maximum of 20 objects are returned (default), you can request the second page by setting `offset` to 20, third page by setting it to 40 and so on.

**`order_by`**
Accepted values are `id`, `user_id`, `status`, `date_added`.

**`sort_order`**
Accepted values are `asc` or `desc`.

**`status`**
Filter invoices by status. Accepted values are:
`0` – Abandoned checkout,
`1` – Paid,
`2` – Overdue (for manually issued invoices),
`3` – Unpaid (for manually issued invoices),
`4` – Refunded,
`5` – Payment pending (e.g. waiting for PayPal fraud checks to clear),
`6` – Partially paid.

**`date_from`**
Filter invoices by creation date. Must be in `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` format, UTC timezone. For example, `2022-12-31 23:59:59`.

**`date_to`**
Filter invoices by creation date. Must be in `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` format, UTC timezone. For example, `2022-12-31 23:59:59`.

**`coupon_id`**
Filter invoices by coupon_id.

**`user_id`**
Filter invoices by customer's user_id.

## Orders

Orders get created automatically as customers buy your services. You can use the API to find and update existing orders. When retrieving or updating an order, the order object is returned. When listing orders, an array of order objects is returned.

### Create an order

```
curl https://username.spp.io/api/v1/orders \
    -u YOUR_API_KEY: \
    -d 'status=1' \
    -d 'employees[]=123' \
    -d 'date_added=2020-12-01 23:00:00' \
    -d 'date_started=2020-12-01 23:00:00' \
    -d 'date_completed=2020-12-01 23:00:00' \
    -d 'date_due=2020-12-01 23:00:00' \
    -d 'service=Test Service' \
    -d 'service_id=123' \
    -d 'note=Order note' \
    -d 'tags[]=Test Tag'
```

**`user_id`** (required)

**`invoice_id`**
If this order is for an existing invoice you can provide the invoice ID (`666AF315`). The order number will be generated to match the invoice.

**`status`**
Order's status ID.

**`employees[]`**
Array of assigned employee's IDs.

**`date_added`**
Order's creation date. It must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_started`**
Order's start date. Order is started when client has filled out your project data form. If no form is needed, order start date is the same as creation date. Value must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_completed`**
Order's completion date for completed orders. Value must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_due`**
Order's due date if purchased service has a deadline. It must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`service`** (required)
Name of purchased service, will be shown in order title.

**`service_id`** (required)
Purchased service's ID.

**`note`**
Order's note. Only visible to your team.

**`tags[]`**
Array of order tags, which will replace the existing tag array.


### Retrieve an order

You only need the order ID.

```
curl https://username.spp.io/api/v1/orders/666AF315 \
    -u YOUR_API_KEY:
```

### Update an order

Any parameters not provided will be left unchanged.

```
curl https://username.spp.io/api/v1/orders/666AF315 \
    -u YOUR_API_KEY: \
    -d 'status=1' \
    -d 'employees[]=123' \
    -d 'date_added=2020-12-01 23:00:00' \
    -d 'date_started=2020-12-01 23:00:00' \
    -d 'date_completed=2020-12-01 23:00:00' \
    -d 'date_due=2020-12-01 23:00:00' \
    -d 'service=Test Service' \
    -d 'service_id=123' \
    -d 'note=Order note' \
    -d 'tags[]=Test Tag'
```

#### Arguments

**`order`** (required)

**`status`**
Order's status ID.

**`employees[]`**
Array of assigned employee's IDs.

**`date_added`**
Order's creation date. It must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_started`**
Order's start date. Order is started when client has filled out your project data form. If no form is needed, order start date is the same as creation date. Value must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_completed`**
Order's completion date for completed orders. Value must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`date_due`**
Order's due date if purchased service has a deadline. It must be in YYYY-MM-DD HH:MM:SS format UTC time.

**`service`**
Name of purchased service.

**`service_id`**
Purchased service's ID.

**`note`**
Order's note. Only visible to your team.

**`tags[]`**
Array of order tags, which will replace the existing tag array.

### Delete an order
Permanently deletes an order, its form data and messages. This action cannot be undone.

```
curl https://username.spp.io/api/v1/orders/666AF315 \
    -u YOUR_API_KEY: \
    -X DELETE
```

### List all orders

```
curl https://username.spp.io/api/v1/orders?limit=3 \
    -u YOUR_API_KEY:
```

#### Arguments

**`limit`**
The maximum number of order objects returned.

## Order Messages

Communication about orders happens in order messages. Listing order messages returns an array of message objects in chronological order. When posting a message, the message object is returned.

### Create an order message

```
curl https://username.spp.io/api/v1/order_messages/666AF315 \
    -u YOUR_API_KEY: \
    -d 'message=<p>This is a client message.</p>' \
    -d 'user_id=123' \
    -d 'staff_only=0'
```

#### Arguments

**`order`** (required)
Order number.

**`message`** (required)
Message HTML body.

**`user_id`** (required)
Message sender.

**`staff_only`**
Set to `1` to send a team-only message.


### Delete an order message
Permanently deletes an order message. This action cannot be undone.

```
curl https://username.spp.io/api/v1/order_messages/666AF315/2 \
    -u YOUR_API_KEY: \
    -X DELETE
```
#### Arguments

**`order`** (required)
Order number.

**`message_id`** (required)
Message ID.

### List all order messages

```
curl https://username.spp.io/api/v1/order_messages/666AF315 \
    -u YOUR_API_KEY:
```

#### Arguments

**`order`** (required)





## Tickets

Tickets are created when somebody emails your support address or fills out a contact form. You can use the API to retrieve, create, update, and delete tickets. When retrieving or updating a ticket, the ticket object is returned. When listing tickets, an array of ticket objects is returned.

### Create a ticket

```
curl https://username.spp.io/api/v1/tickets \
    -u YOUR_API_KEY: \
    -d 'user_id=2' \
    -d 'subject=Test Ticket' \
    -d 'status=1' \
    -d 'note=Ticket note' \
```

#### Arguments
**`user_id`**
User ID for whom a ticket is created

**`status`**
Ticket's status ID.

**`note`**
Ticket's note. Only visible to your team.

**`subject`**
Ticket's subject.


### Retrieve a ticket

You only need the ticket ID.

```
curl https://username.spp.io/api/v1/tickets/666AF315 \
    -u YOUR_API_KEY:
```

### Update a ticket

Any parameters not provided will be left unchanged.

```
curl https://username.spp.io/api/v1/tickets/666AF315 \
    -u YOUR_API_KEY: \
    -d 'status=1' \
    -d 'note=Ticket note' \
    -d 'subject=Test Ticket' \
    -d 'employees[]=123' \
    -d 'tags[]=Test Tag'
```

#### Arguments

**`ticket`** (required)

**`status`**
Ticket's status ID.

**`note`**
Ticket's note. Only visible to your team.

**`subject`**
Ticket's subject.

**`employees[]`**
Array of assigned employee's IDs.

**`tags[]`**
Array of ticket tags, which will replace the existing tag array.

### Delete a ticket
Permanently deletes a ticket, its form data and messages. This action cannot be undone.

```
curl https://username.spp.io/api/v1/tickets/666AF315 \
    -u YOUR_API_KEY: \
    -X DELETE
```

### List all tickets

```
curl https://username.spp.io/api/v1/tickets?limit=3 \
    -u YOUR_API_KEY:
```

#### Arguments

**`limit`**
The maximum number of ticket objects returned.

## Ticket Messages

Listing ticket messages returns an array of message objects in chronological order. When posting a message, the message object is returned.

### Create a ticket message

```
curl https://username.spp.io/api/v1/ticket_messages/666AF315 \
    -u YOUR_API_KEY: \
    -d 'message=<p>This is a client message.</p>' \
    -d 'user_id=123' \
    -d 'staff_only=0'
```

#### Arguments

**`ticket`** (required)
Ticket number.

**`message`** (required)
Message HTML body.

**`user_id`** (required)
Message sender.

**`staff_only`**
Set to `1` to send a team-only message.


### Delete a ticket message
Permanently deletes a ticket message. This action cannot be undone.

```
curl https://username.spp.io/api/v1/ticket_messages/666AF315/2 \
    -u YOUR_API_KEY: \
    -X DELETE
```
#### Arguments

**`ticket`** (required)
Ticket number.

**`message_id`** (required)
Message ID.

### List all ticket messages

```
curl https://username.spp.io/api/v1/ticket_messages/666AF315 \
    -u YOUR_API_KEY:
```

#### Arguments

**`ticket`** (required)

## Login Links

Magic login links allow your clients to log into your Client Portal with one click. They can be generated for any customer and expire in 15 minutes. A magic login link can only be used once.

### Retrieve a login link

```
curl https://username.spp.io/api/v1/login_links/2 \
    -u YOUR_API_KEY:
```

#### Arguments

**`client`** (required)

## PHP Example

Here's an example API call using [Guzzle](http://docs.guzzlephp.org/en/stable/). Replace `username.spp.io` with your account's URL and `spp_api_xxx` with your API key.

```
#!php
<?php
 require 'vendor/autoload.php';

 $guzzle = new GuzzleHttp\Client([
     'base_uri' => 'https://username.spp.io/api/v1/',
     'auth' => ['spp_api_xxx', null],
 ]);

 try {
     $request = $guzzle->request('POST', 'clients', [
         'form_params' => [
             'email' => 'test@example.com',
         ]
     ]);

     $data = json_decode($request->getBody());

     // Do something with the response data
     var_dump($data);

 } catch (Exception $e) {
     // Handle request exception
 }
```

## Node.js Example
```javascript
let apiKey = 'spp_api_xxx';
let formData = new FormData();

formData.append('email', 'test@example.com');

fetch('https://username.spp.io/api/v1/clients', {
        method: 'POST',
        headers: {
            Authorization:'Basic ' + btoa(apiKey + ':')
        },
        body: formData
    })
    .then(response => response.json())
    .then(json => console.log(json));
```

## Changelog

-   27-04-2021 – updated examples.
-   19-04-2021 – added endpoint for `order` creation.
-   12-06-2020 – renamed the `messages` endpoint to `order_messages` and added endpoints for `tickets`, `ticket_messages`, and `login_links`.
-   23-03-2020 – added `coupon_id` and `status` arguments to invoices endpoint. Added `email` and `status` filters to accounts listing. Added documentation for using `offset` in pagination.
-   06-02-2020 – added `DELETE` methods.
-   06-01-2020 – added functions for updating, charging, and marking invoices as paid.
-   13-09-2019 – replaced `employee` property in `order` object with array of `employees`.

## More Information

-   [Check out our Help Center](https://spp.co/help)
-   [Contact support](https://spp.co/help/contact/)
-   [View system status](https://status.spp.co)
